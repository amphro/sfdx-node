// @salesforce/command is brought in by salesforcedx and we want to use the same
// version to properly replace the correct prototype.
// npm > should resolve this.
const { SfdxCommand } = require('@salesforce/command');
// We want consumers of this library to be able to catch command errors, so diable
// SfdxCommand's error output
SfdxCommand.prototype.catch = err => { throw err; };

const debug = require('debug')('sfdx-node');
const { get, set } = require('lodash');
const OclifConfig = require('@oclif/config');

/**
 * Convert flags and args object to argv string array that oclif needs
 */
function convertToArgv(flags = {}, args = []) {
  const argv = [];
  Object.keys(flags).forEach(key => {
    const val = flags[key];
    // oclif boolean flags don't support values so ignore
    // e.g. "--synchronous false" is not valid
    if (val !== false) {
      argv.push(`--${key}`);
      if (val !== true) {
        argv.push(String(val));
      }
    }
  });
  args.forEach(arg => {
    argv.push(arg);
  });
  return argv;
}

/**
 * Run an oclif command. **Note:** Suppressed stdout and stderr of the command
 */
async function run(command, flags = {}, args = []) {
  // Json should always be set, since we want the programatic response
  flags.json = true;

  command = await command.load();
  const argv = convertToArgv(flags, args);
  debug(`*** Running command: ${command.id} ${argv.join(' ')}`);

  const code = process.exitCode;
  const stdout = process.stdout.write;
  const stderr = process.stderr.write;

  // Write to buffer if you need output, but you shouldn't since we want command data
  process.stdout.write = () => {};
  process.stderr.write = () => {};

  try {
    // If there is some reason the prerun hook ever needs to be ran, add it here.
    // You will probably want this to support plugin hooks, but importing
    // salesforcedx like this won't bring in any installed plugins. Also note
    // that oConfig needs to be passed in
    // await oConfig.runHook('prerun', { Command: command, argv });
    return await command.run(argv);
  } finally {
    // Set back for the rest of the process
    process.exitCode = code;
    process.stdout.write = stdout;
    process.stderr.write = stderr;
  }
}

const sfdx = {};

sfdx.init = async function() {
  const commandsDir = require.resolve('salesforcedx');

  debug(`Commands dir: ${commandsDir}`);
  const oConfig = await OclifConfig.load(commandsDir);
  for (const command of oConfig.commands) {
    const cmdPath = command.id.split(':').join('.');
    const runFn = async (flags, args) => run(command, flags, args);

    if (!get(sfdx, cmdPath)) {
      set(sfdx, cmdPath, {});
    }
    // There can be topic commands and subcommands, so we have to set a
    // run method so we don't override those commands. i.e.
    // sfdx force:mdapi:deploy and sfdx force:mdapi:deploy:report
    //
    // TODO explore setting the next command on the function object
    // So you can still have `sfdx.force.mdapi.deploy()` AND `sfdx.force.mdapi.deploy.report()`
    // But this complicates the code from using get/set.
    get(sfdx, cmdPath).run = runFn;
  }

  // Don't reload a  second time
  sfdx.init = () => sfdx;
  return sfdx;
};

module.exports = sfdx;
