var assert = require('chai').assert;
var sfdx = require('../');

describe('sfdx', function () {
  before(async () => {
    await sfdx.init();
  });

  it('should have object \'org\'', () => {
    assert.isObject(sfdx.force.org);
  });
  describe('org', function () {
    it('should have function \'list\'', function () {
      assert.isFunction(sfdx.force.org.list.run);
    });
    it('should have function \'open\'', function () {
      assert.isFunction(sfdx.force.org.open.run);
    });
    it('should have function \'mdapi deploy\'', function () {
      assert.isFunction(sfdx.force.mdapi.deploy.run);
    });
    it('should have function \'mdapi  deploy report\'', function () {
      assert.isFunction(sfdx.force.mdapi.deploy.report.run);
    });
  })

  describe('sfdx command execution', function () {
    it('should reject when trying to display a non-existing org', () => {
      return sfdx.force.org.display.run({
        targetusername: 'wrong@username'
      })
      .then(() => {
        throw new Error('Expected method call to reject.');
      })
      .catch((err) => {
        assert.include(err.message, 'wrong@username', 'Did not receive expected error from the method call.');
      });
    });
  });

  describe('sfdx parallel command execution', function () {
    this.timeout(50000);
    it('Without parallel execution, all parallel calls should fail with the same error, even if only one has an actual error', function () {
      const x1 = sfdx.force.org.display.run({
        targetusername: 'wrong@username'
      })
        .then(() => {
          throw new Error('Expected first method call to reject.');
        })
        .catch((err1) => {
          if (err1 instanceof Error && err1.message === 'Expected first method call to reject.') {
            throw err1;
          }
          assert.include(err1.message, 'wrong@username', 'Did not receive expected error from the method call.');
          return err1;
        });

      const x2 = sfdx.force.config.list.run()

      return Promise.all([x1, x2])
        .then(([x1Result, x2Result]) => {
          assert.include(x1Result.message, 'wrong@username', 'Did not receive expected error from the method call.');
          assert.isArray(x2Result);
        });
    });
  });
});
