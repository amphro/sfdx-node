const assert = require('chai').assert;
const sfdx = require('../');
const shell = require('shelljs');

describe('sfdx', () => {
  let testDir;
  before(async () => {
    assert.isString(process.env.SFDX_NODE_TEST_DEV_HUB, 'SFDX_NODE_TEST_DEV_HUB is not set, need a dev hub to run these tests');
    testDir = `tmp-${Math.random().toString(36).substring(2, 15)}`;
    
    shell.mkdir(testDir);
    shell.cd(testDir);
    await sfdx.init();
    await sfdx.force.project.create.run({
      template: 'standard',
      projectname: testDir,
      outputdir: testDir
    });
  });

  after(async () => {
    shell.cd('../');
    shell.rm('-rf', testDir);

    try {
      // Always try to delete the created org
      await sfdx.force.org.delete.run({
        targetusername: 'sfdx-node-e2e',
        noprompt: true
      });
      console.log('deleted sfdx-node-e2e org');
    } catch(e) {
      console.error(e);
    }
  });

  it('e2e', async () => {
    const orgInfo = await sfdx.force.org.create.run({
      targetdevhubusername: process.env.SFDX_NODE_TEST_DEV_HUB,
      definitionfile: 'config/project-scratch-def.json',
      setalias: 'sfdx-node-e2e'
    });
    console.log(orgInfo);
    // TODO Should create some apex, do a deploy, then do a retrieve and apex test in parrellel
  });
});