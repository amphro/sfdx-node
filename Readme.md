# SFDX-Node

Wrapper for the Salesforce CLI to use in Node.js

## Installation
`npm install sfdx-node`

## Usage
 The Salesforce CLI is based on OCLIF. To use the CLI commands within node we wrap the topics, and camelcase the commands. For example `sfdx force:auth:web:login` becomes `sfdx.force.auth.webLogin.run()`.

 > sfdx force:TOPIC:COMMAND:SUB -> sfdx.topic.commandSub.run();

 The run method must be called because sub-commands on a command. For example, `sfdx force:mdapi:deploy` and `sfdx force:mdapi:deploy:report`.

 Additionally option flags can be passed in as an object to the command using the flag names. So `sfdx force:org:create --setdefaultusername` becomes `sfdx.force.org.create.run({ setdefaultusername: true})`. Arguments are the next parameter. So `sfdx force:org:create edition=Developer` becomes `sfdx.force.org.create.run({}, ['edition=Developer'])`

 Commands all return a JS Promise.

```javascript
const sfdx = require('sfdx-node');

//authorize a dev hub
sfdx.force.auth.webLogin.run({
  setdefaultdevhubusername: true,
  setalias: 'HubOrg'
})
  .then(() => sfdx.force.source.push.run()) //push source
  .then((results) => {
    // Display confirmation of source push
    console.log('Source pushed to scratch org', results);
  });
```

### Promise rejection overlapping for parallel calls

When multiple CLI commands are executed in parallel using this module, while also making use of `rejectOnError`, more than one or even all the commands may end up rejecting the promises. This can happen because the errors are shared among all the parallel executions.

To avoid this, make use of `sfdx-node/parallel` module. It works in the same way as the main module, except for the fact that each command is executed in it's own child process. This ensures that each command execution has it's own context and it doesn't share errors with other commands executing in parallel.

```javascript
const sfdx = require('sfdx-node/parallel');

// Get source status for first scratch org
sfdx.source.status({
  targetusername: 'test-user1@example.com',
  rejectOnError: true
})
  .then((statusResult) => {
    // Source status for first scratch org
    console.log('First org source status:', statusResult);
  })
  .catch((statusError) => {
    // Error occurred during source status check for first scratch org
    console.log('First org error:', statusError);
  });

// Get source status for second scratch org
sfdx.source.status({
  targetusername: 'test-user2@example.com',
  rejectOnError: true
})
  .then((statusResult) => {
    // Source status for second scratch org
    console.log('Second org source status:', statusResult);
  })
  .catch((statusError) => {
    // Error occurred during source status check for second scratch org
    console.log('Second org error:', statusError);
  });
```
