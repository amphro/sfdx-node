const { fork } = require('child_process');
const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const sfdx = require('salesforce-alm');

const _createCommand = (topic, command) => (flags, opts) => new Promise((resolve, reject) => {
  const childArgs = {
    topic,
    command,
    flags,
    opts
  };
  const child = fork(path.join(__dirname, 'child.js'), [], { stdio: 'pipe' });
  child.stderr.on('data', (data) => {
    console.log(data.toString());
  });
  child.stdout.on('data', (data) => {
    console.log(data.toString());
  });
  child.on('message', (message) => {
    if (message.type === 'resolved') {
      resolve(message.value);
    } else {
      reject(message.value);
    }
  });
  child.send({
    cmd: "SFDX_PARALLEL_init",
    args: childArgs
  });
});

const sfdxApi = {};
const commandsByTopic = _.groupBy(sfdx.commands, 'topic');
_.forEach(commandsByTopic, (commands, topic) => {
  const sfdxTopic = sfdxApi[topic] = {};
  _.forEach(commands, (command) => {
    sfdxTopic[_.camelCase(command.command)] = _createCommand(topic, _.camelCase(command.command));
  });
});

module.exports = sfdxApi;
