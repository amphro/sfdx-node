const _ = require('lodash');

const getPlainObjectFromNativeError = (err) => {
  const error = {};
  if (err instanceof Error) {
    let obj = err;
    const props = [];
    do {
      Object.getOwnPropertyNames(obj).forEach((key) => {
        if (!props.includes(key) && key !== '__proto__' && typeof obj[key] !== 'function') {
          error[key] = obj[key];
          props.push(key);
        }
      });
    } while (obj = Object.getPrototypeOf(obj));
  }
  return error;
};

const processSingleError = (inputError) => {
  let outputError = {
    message: '',
  };
  if (inputError instanceof Error) {
    outputError = getPlainObjectFromNativeError(inputError);
  } else if (inputError && inputError.error && inputError.error instanceof Error) {
    outputError = getPlainObjectFromNativeError(inputError.error);
  } else if (_.isPlainObject(inputError) && _.has(inputError, 'message')) {
    outputError = inputError;
  } else if (_.isPlainObject(inputError) && _.isPlainObject(inputError.error) && _.has(inputError.error, 'message')) {
    outputError = inputError.error;
  } else if (typeof inputError !== 'string') {
    const str = String(inputError);
    if (str !== '[object Object]') {
      outputError.message = str;
    } else {
      outputError.message = JSON.stringify(inputError);
    }
  } else {
    outputError.message = inputError;
  }
  return outputError;
};

const processAllErrors = (sfdxErrors) => {
  let errors = [];
  if (Array.isArray(sfdxErrors)) {
    errors = sfdxErrors.map(currError => processSingleError(currError));
  } else {
    errors.push(processSingleError(sfdxErrors));
  }
  return errors;
};

module.exports = {
  processSingleError,
  processAllErrors,
};
